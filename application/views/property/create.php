<?php // include('../../includes/body.php');   ?>

<script type="text/javascript">
    $(document).ready(function () {
        var frmProperty = $('#frmProperty');

        frmProperty.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error',
            ignore: "", // validate all fields including form hidden input
            rules: {
                varPropertyName: {
                    required: true,
                },
                'varImage[]': {
                    required: true,
                },
                varAddress: {
                    required: true,
                },
                varLocality: {
                    required: true,
                },
                decPrice: {
                    required: true,
                    minlength : 5,
                },
                intBedRoom: {
                    required: true,
                },
                intBath: {
                    required: true,
                },
                varCarpetArea: {
                    required: true,
                },
            },
            messages: {
                varPropertyName: {
                    required: 'Please enter property name.',
                },
                varAddress: {
                    required: 'Please enter address.',
                },
                varLocality: {
                    required: 'Please enter locality/area.',
                },
                decPrice: {
                    required: 'Please enter price.',
                    minlength: 'Please enter at least 5 digits.'
                },
                intBedRoom: {
                    required: 'Please select bedroom.',
                },
                intBath: {
                    required: 'Please select bath.',
                },
                varCarpetArea: {
                    required: 'Please enter the carpet area.',
                },

            },
            errorPlacement: function (error, element) {
                if ($(element).attr('id') == 'txtDescription') {
                    $('#texterror').addClass('has-error')
                    error.appendTo($('#texterror'));
                } else if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                if ($("#varImage")[0].files.length > 5) {
                  alert("You can select only 5 images");
                }else{
                    form.submit();
                }            
            }
        });
    });
    function DeleteFile() {
        var conf = confirm("The selected image will be deleted. Press OK to confirm");
        if (conf == true) {
            $("#hiduserfile").val('');
            $("#divdelbro").html('');
        }
    }
    function ShowHideDivs(val) {
        if (val == 'C') {
            $('#trimmingSection').hide();
            $('#bitrateTypeContainer').show();
        } else if (val == 'T') {
            $('#trimmingSection').show();
            $('#bitrateTypeContainer').hide();
        } else {
            $('#trimmingSection').hide();
            $('#bitrateTypeContainer').hide();
        }
    }

    // $(function(){
    //   $('.datetimepicker1').datetimepicker({
    //       format: 'hh:mm:ss'
    //   });
    // });
    function KeycheckOnlyForPrice(e){
        var t = 0;
        t = document.all ? 3 : document.getElementById ? 1 : document.layers ? 2 : 0;
        if (document.all)
            e = window.event;
            var n = "";
            var r = "";  
       
        if (t == 2) {
            if (e.which > 0)
                n = "(" + String.fromCharCode(e.which) + ")";
            r = e.which
        }else {
            if (t == 3) {
                r = window.event ? event.keyCode : e.which
            } else {
                if (e.charCode > 0)
                    n = "(" + String.fromCharCode(e.charCode) + ")";
                r = e.charCode
            }
        }
        if ((r >= 30 && r <= 39) || (r >= 42 && r<=47 && r != 46) || (r >= 58 && r <= 90) || (r >= 91 && r <= 126 )){
                    return false
        }
        return true
    }

    function onlyNumbersWithColon(e) {
        var charCode;
        if (e.keyCode > 0) {
            charCode = e.which || e.keyCode;
        } else if (typeof (e.charCode) != "undefined") {
            charCode = e.which || e.keyCode;
        }
        if (charCode == 58)
            return true
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>

<div class="Header_Part">
    <div class=""> 
        <div class="page-header-inner">                  
            <div class="menu-toggler sidebar-toggler hide"> </div>
        </div>
        <a href="javascript:void(0);" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a> 
    </div>
</div>
<div class="clearfix"> </div>
</div>

<div class="container">
    <div class="page-content">
        <h2 class="page-title pull-left">Add</h2>
        <div class="clearfix"> </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="portlet form">
                    <form action="<?php echo base_url('propertyCreate');?>" name="frmProperty" id="frmProperty" method="POST" enctype="multipart/form-data">                           
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Property Name<span aria-required="true" class="required"> * </span> </label>

                                        <div class="input-group">
                                            <input type="text" name="varPropertyName" value="" id="varPropertyName" maxlength="30" class="form-control input-medium search-input combo-space" >
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Discription</label>
                                        <div class="input-group">
                                            <input type="text" name="varDescription" value="" id="varDescription" maxlength="100" class="form-control input-medium search-input combo-space" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Home Image Upload Start -->
                            <div id="ImageDiv">
                                <div class="row">
                                    <div class="form-group">                                    
                                        <div class="col-lg-6">
                                            <label class="control-label">Image Upload<span aria-required="true" class="required"> * </span></label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="input-group">
                                                    <div class="form-control uneditable-input" data-trigger=""> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
                                                    <span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
                                                      <?php $imagevalue = isset($Result['varImage']) ? $Result['varImage'] : ''; ?>                                                        
                                                      <input type="file" name="varImage[]" multiple="multiple" id="varImage" value = "">
                                                    </span> <a href="javascript:void(0);" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> 
                                                </div>
                                                <span class="help-block help-block-error" for="varImage"></span>
                                            </div>
                                            
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <!-- Home Image Upload End -->                           

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Address<span aria-required="true" class="required"> * </span></label>
                                        <div class="input-group">
                                            <textarea id="varAddress" name="varAddress" rows="4" cols="50">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Locality/Area<span aria-required="true" class="required"> * </span></label>
                                        <div class="input-group">
                                            <input type="text" name="varLocality" value="" id="varLocality" maxlength="30" class="form-control input-medium search-input combo-space" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Price<span aria-required="true" class="required"> * </span></label>
                                        <div class="input-group">
                                            <input type="text" name="decPrice" value="" id="decPrice" maxlength="30" class="form-control input-medium search-input combo-space" onkeypress= 'return KeycheckOnlyForPrice(event)'>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">BedRoom<span aria-required="true" class="required"> * </span></label>
                                        <div class="input-group">                                            
                                            <select name="intBedRoom" class="form-control input-medium pull-left combo-space bs-select input-filter" id="intBedRoom">
                                              <option value="">Select BedRoom</option>
                                              <?php for($i = 1; $i <= 10; $i++){ ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                              <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Bath<span aria-required="true" class="required"> * </span></label>
                                        <div class="input-group">                                            
                                            <select name="intBath" class="form-control input-medium pull-left combo-space bs-select input-filter" id="intBath">
                                              <option value="">Select Bath</option>
                                              <?php for($i = 1; $i <= 10; $i++){ ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                              <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">

                                        <label class="control-label">Carpet Area<span aria-required="true" class="required"> * </span></label>
                                        <div class="input-group">
                                            <input type="text" name="varCarpetArea" value="" id="varCarpetArea" maxlength="30" class="form-control input-medium search-input combo-space" >
                                        </div>
                                        <span class="help-block"><b>(Carpet Area Like: Sq Ft, Sq Yd, Sq Mt.)</b></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="">
                                        <input type="submit" name="btnsaveandc" value="Save &amp; Keep Editing" class="btn blue btm-10 right-5" title="Save &amp; Keep Editing" id="addvideo">
                                        <div class="clearfix visible-xs-small"></div>
                                        <a href="<?php echo base_url('property');?>" class="btn red">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>

</div>
