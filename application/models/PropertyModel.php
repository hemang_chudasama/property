<?php

class PropertyModel extends CI_Model {

    public function get_property() {
        $this->db->select('P.intGlCode,P.varPropertyName');
        $this->db->from("property as P");
        $this->db->where('P.chrDelete', 'N');
        $this->db->limit(DEFAULT_LIMIT);
        $this->db->order_by('P.intGlCode', 'DESC');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $res = $query->result_array();
        return $res;
    }

    public function getPropertyById($id) {
        $this->db->select('*');
        $this->db->from("property");
        $this->db->where('chrDelete', 'N');
        $this->db->where('intGlCode', $id);
        $query = $this->db->get();;
        $res = $query->row_array();
        return $res;
    }

    public function getPropertySearch(){
        $searchByLocality = $this->input->post('searchByLocality');
        $decPriceRange = $this->input->post('decPriceRange');
        $intBedRoom = $this->input->post('intBedRoom');

        $this->db->select('P.intGlCode,P.varPropertyName,P.varLocality,P.varAddress,P.decPrice,P.intBath');
        $this->db->from("property as P");
        if (!empty($searchByLocality)) {
            $this->db->like('varLocality', $searchByLocality);
            // $this->db->or_like('description', $this->input->get("search"));
        }
        if(!empty($decPriceRange)){
            $priceRangeArry = explode('-',$decPriceRange);
            $startPrice = strip_tags($priceRangeArry[0]);
            $startPrice = substr($startPrice, 0,-1);
            $endPrice = strip_tags($priceRangeArry[1]);
            $endPrice = substr($endPrice, 0,-1);             
        }

        if(!empty($decPriceRange) && !empty($startPrice) && !empty($endPrice)){
            $startPrice = $startPrice * 100000;
            $endPrice = $endPrice * 100000;
            $where = "P.decPrice BETWEEN ".$startPrice." AND ".$endPrice." ";
            $this->db->where($where);
        }

        if(!empty($intBedRoom)){
            $this->db->where('P.intBedRoom', $intBedRoom);
        }
        $this->db->where('P.chrDelete', 'N');
        $this->db->order_by('P.intGlCode', 'DESC');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $res = $query->result_array();
        return $res;
    }

    public function recentlyViewedProperty(){
        $this->db->select('P.intGlCode,P.varPropertyName');
        $this->db->from("property as P");
        $this->db->where('P.chrDelete', 'N');
        $this->db->where('P.intViewCount >', 1);
        $this->db->limit(RECENTLY_DEFAULT_LIMIT);
        $this->db->order_by('P.intGlCode', 'DESC');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $res = $query->result_array();
        // echo "<pre>";
        // print_r($res);die;
        return $res;   
    }

    public function get_property_images($id="",$limit=""){        
        $this->db->select('PI.intGlCode as varImageId,PI.varImageName');
        $this->db->from("propertyImage as PI");
        $this->db->where('PI.chrDelete', 'N');
        if(!empty($id)){
            $this->db->where('PI.fkImageId', $id);
        }
        if(!empty($limit)){
            $this->db->limit($limit);
        }
        $this->db->order_by('PI.intGlCode', 'ASC');
        $query = $this->db->get();
        $res = $query->row_array();
        return $res;
    }

    public function getAllPropertyImages($id){
        $this->db->select('PI.intGlCode,PI.varImageName');
        $this->db->from("propertyImage as PI");
        $this->db->where('PI.chrDelete', 'N');
        if(!empty($id)){
            $this->db->where('PI.fkImageId', $id);
        }
        $this->db->order_by('PI.intGlCode', 'ASC');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function addViewCount($id){
        $this->db->set('intViewCount', 'intViewCount+1', FALSE);
        $this->db->set('dtModifyDate', date("Y-m-d H:i:s"));

        $this->db->where('intGlCode', $id);
        $this->db->update('property');
        // echo $this->db->last_query();die;
        return true;
    }


    public function addFavorite($id){
        $this->db->set('intFavorite', 1);
        $this->db->set('dtModifyDate', date("Y-m-d H:i:s"));
        $this->db->where('intGlCode', $id);
        $this->db->update('property');
        // echo $this->db->last_query();die;
        return true;
    }

    public function getViewCount($id){
        $this->db->select('P.intGlCode,P.intViewCount');
        $this->db->from("property as P");
        $this->db->where('P.chrDelete', 'N');
        $this->db->where('P.intGlCode', $id);
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $res = $query->row_array();
        return $res['intViewCount'];
    }

    public function insertPropertyData() {
        $image = array();
        $dtCreateDate = date('Y-m-d H:i:s');
        $ImageCount = count($_FILES['varImage']['name']);

        $lastId = $this->checkLastId();
        if (empty($lastId) || $lastId == 0) {
            $lastId = 0;
        }
        $uploadId = ($lastId + 1) * 100;
        mkdir('property_image/'.$uploadId); // If permission is not allow.
        // $CI = &get_instance();
        for($i = 0; $i < $ImageCount; $i++){
            $_FILES['file']['name']       = $_FILES['varImage']['name'][$i];
            $_FILES['file']['type']       = $_FILES['varImage']['type'][$i];
            $_FILES['file']['tmp_name']   = $_FILES['varImage']['tmp_name'][$i];
            $_FILES['file']['error']      = $_FILES['varImage']['error'][$i];
            $_FILES['file']['size']       = $_FILES['varImage']['size'][$i];

            // File upload configuration            
            $uploadPath = 'property_image/'.$uploadId;
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'jpg|jpeg|png|gif';

            // Load and initialize upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // $this->thumb($_FILES['varImage']['name'][$i],100,100,$uploadPath);

            // Upload file to server
            if($this->upload->do_upload('file')){
                // Uploaded file data
                $imageData = $this->upload->data();
                $uploadImgData[$i]['image_name'] = $imageData['file_name'];
            }
        }        

        $data = array(
            'varPropertyName' => trim($this->input->post('varPropertyName')),
            'varDescription' => trim($this->input->post('varDescription')),
            'varAddress' => trim($this->input->post('varAddress')),
            'varLocality' => trim($this->input->post('varLocality')),
            'decPrice' => $this->input->post('decPrice'),
            'intBedRoom' => $this->input->post('intBedRoom'),
            'varCarpetArea' => trim($this->input->post('varCarpetArea')),
            'intBedRoom' => $this->input->post('intBedRoom'),
            'intBath' => $this->input->post('intBath'),            
            'dtCreateDate' => $dtCreateDate,
        );

        $this->db->insert('property', $data);
        $id = $this->db->insert_id();
        
        if(!empty($uploadImgData)){
            for($i = 0; $i < count($uploadImgData); $i++){
                $image_name = $uploadId.'/'.$uploadImgData[$i]['image_name'];
                $imageData = array(
                    'fkImageId' => $id,
                    'varImageName' => $image_name,
                    'dtCreateDate' => $dtCreateDate,
                );
                // Insert files data into the database
                $this->db->insert('propertyImage', $imageData);
            }            
        }
        return $id;
    }

    public function thumb($fullname, $width, $height,$path) {
        // Path to image thumbnail in your root
        $dir = $path;
        $url = base_url().$path;
        // Get the CodeIgniter super object
        $CI = &get_instance();
        // get src file's extension and file name
        $extension = pathinfo($fullname, PATHINFO_EXTENSION);
        $filename = pathinfo($fullname, PATHINFO_FILENAME);
        $image_org = $dir . $filename . "." . $extension;
        $image_thumb = $dir . $filename . "-" . $height . '_' . $width . "." . $extension;
        $image_returned = $url . $filename . "-" . $height . '_' . $width . "." . $extension;

        if (!file_exists($image_thumb)) {
            // LOAD LIBRARY
            $CI->load->library('image_lib');
            // CONFIGURE IMAGE LIBRARY
            $config['source_image'] = $image_org;
            $config['new_image'] = $image_thumb;
            $config['width'] = $width;
            $config['height'] = $height;
            $CI->image_lib->initialize($config);
            $CI->image_lib->resize();
            $CI->image_lib->clear();
        }
        return $image_returned;
    }
    public function checkLastId(){
        $this->db->select('MAX(intGlCode) AS Id');
        $this->db->from("property");
        $this->db->where('chrDelete', 'N');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $res = $query->row_array();
        return $res['Id'];
    }

    public function countData($id){
        $this->db->select('count(intGlCode) AS num_rows');
        $this->db->from("property");
        $this->db->where('chrDelete', 'N');
        $this->db->where('intGlCode <', $id);
        $this->db->order_by('intGlCode', 'DESC');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $res = $query->row_array();
        return $res['num_rows'];   
    }

    public function loadMoreDate($id){
        $this->db->select('*');
        $this->db->from("property");
        $this->db->where('chrDelete', 'N');
        $this->db->where('intGlCode <', $id);
        $this->db->order_by('intGlCode', 'DESC');
        $this->db->limit(DEFAULT_LIMIT);
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $res = $query->result_array();
        return $res;   
    }

}

?>