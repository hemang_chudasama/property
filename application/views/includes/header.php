<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8"/>
      <title>Property</title> 

      <!--Start-->      
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>   
      <link href="<?php echo SITE_PATH."css/style.css?v=1122021"; ?>" rel="stylesheet" type="text/css"/>
      <link href="<?php echo SITE_PATH."css/lightslider.css"; ?>" rel="stylesheet" type="text/css"/>
      <link href="<?php echo SITE_PATH."css/components.css?v=10022021"; ?>" rel="stylesheet" type="text/css"/>
      
      <link href="<?php echo SITE_PATH."css/custom.css?v=10022021"; ?>" rel='stylesheet' type='text/css'/>

      <link href="<?php echo SITE_PATH."css/bootstrap-fileinput.css"; ?>" rel="stylesheet" type="text/css"/>

      <script type="text/javascript" src="<?php echo SITE_PATH."js/jquery.min.js"; ?>"></script>

      <script type="text/javascript" src="<?php echo SITE_PATH."js/jquery.validate.js"; ?>"></script>
      <script type="text/javascript" src="<?php echo SITE_PATH."js/lightslider.js"; ?>"></script>

      <script type="text/javascript" src="<?php echo SITE_PATH."js/validation_additional-methods.js"; ?>"></script>

      <script type="text/javascript" src="<?php echo SITE_PATH."js/bootstrap-fileinput.js"; ?>"></script>
      <!--End-->     
   </head>
<body>
<div class="container">