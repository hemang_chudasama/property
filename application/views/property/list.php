<script type="text/javascript">
  $(document).ready(function () {
      // $("#intBedRoom").select2({});
      // $(document).on('click','.show_more',function(){
      //   var ID = $(this).attr('id');
      //   $('.show_more').hide();
      //   $.ajax({
      //       type: "POST",
      //       url: "<?php echo SITE_PATH.'showmore/'; ?>",
      //       data: 'id='+ID,
      //       async: false,
      //       success: function(data){
      //         $('#show_more_main'+ID).remove();
      //         $('.loadMoreData').append(data);
      //       }
      //   })
      // });

      $(window).scroll(function(){
          var ID = $('.load-more').attr('data-id');
          if(($(window).scrollTop() == $(document).height() - $(window).height()) && (ID != 0)){
              $.ajax({
                  type:'POST',
                  url: "<?php echo SITE_PATH.'showmore/'; ?>",
                  data: 'id='+ID,
                  async: false,
                  beforeSend:function(){
                      $('.load-more').show();
                  },
                  success:function(html){
                      // $('.load-more').remove();
                      $('.loadMoreData').append(html);
                  }
              });
          }
      });

      $(document).on('keyup','#varSearchByLocality',function(){
          var varSearchByLocality = $('#varSearchByLocality').val();
          console.log('varSearchByLocality ='+varSearchByLocality);
          if (varSearchByLocality.length >= 2 || varSearchByLocality.length < 1) {
            onSearch();
          }          
      });
      $(document).on('change','#intBedRoom',function(){
          onSearch();
      });
      $(document).on('change','#decPriceRange',function(){
          onSearch();
      });
  });

  function onSearch(){
    var searchByLocality = $('#varSearchByLocality').val();
    var decPriceRange = $('#decPriceRange').val();    
    var intBedRoom = $('#intBedRoom').val();   

    $.ajax({
      type: "POST",
      url: "<?php echo SITE_PATH.'search/'; ?>",
      data: 'searchByLocality='+searchByLocality+'&decPriceRange='+decPriceRange+'&intBedRoom='+intBedRoom,
      async: false,
      success: function(data){
        if(data != '' && data != undefined){
          $('#searchSection').html(data);  
        }        
      }
    })
  }

  function addViewCnt(id) {
    $.ajax({
        type: "POST",
        url: "<?php echo SITE_PATH.'addViewCnt/'; ?>",
        data: 'id='+id,
        async: false,
        success: function(data){}
    })
  }  
  
  function addFavorite(id) {
    $.ajax({
        type: "POST",
        url: "<?php echo SITE_PATH.'addFavorite/'; ?>",
        data: 'id='+id,
        async: false,
        success: function(data){
          if(data > 0)
          alert('Your property has been added successfully in favorite.');
        }
    })
  }

  function viewCnt(id){
    $.ajax({
        type: "POST",
        url: "<?php echo SITE_PATH.'getViewCnt/'; ?>",
        data: 'id='+id,
        async: false,
        success: function(data){
          if(data > 0){
            alert('This property is viewed by '+data+' times.');  
          }else{
            alert('This property is no view by any customers.');  
          }
        }
    })
  }
</script>
<div class="row">
    <div class="col-lg-12">           
        <h2>Property Listing           
            <div class="pull-right">
               <a class="btn btn-primary" href="<?php echo SITE_PATH.'property/create'; ?>"> Add Property</a>
            </div>
        </h2>
     </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-4">
        <strong>Search By Locality</strong>
        <div class="row">          
          <input type="text" name="varSearchByLocality" class="form-control input-medium search-input combo-space" id="varSearchByLocality">
        </div>
        <strong>Price Range</strong>
        <div class="row">          
          <select name="decPriceRange" class="form-control input-medium pull-left combo-space bs-select input-filter" id="decPriceRange">
            <option value="">Select Range</option>
            <option value="10L-15L">10L-15L</option>
            <option value="15L-20L">15L-20L</option>
            <option value="25L-30L">25L-30L</option>
          </select>
        </div>
        <strong>Bed Room</strong>
        <div class="row">
          <select name="intBedRoom" class="form-control input-medium pull-left combo-space bs-select input-filter" id="intBedRoom">
            <option value="">Select BedRoom</option>
            <?php for($i = 1; $i <= 10; $i++){ ?>
              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php } ?>
          </select>
        </div>
    </div>
    <div id="searchSection">
      <div class="col-lg-4">
        <div class="loadMoreData">
        <?php
        if(!empty($data)){
        foreach ($data as $details) { 
          $propertyImages = $this->Module_Model->get_property_images($details['intGlCode'],1);        
        ?>      
        <div><img src="<?php echo SITE_PATH.PROPERTY_IMAGES_PATH.$propertyImages['varImageName']; ?>" width="100px" height="100px"></div>
        <div class="property-name"><b>Property Name: </b><?php echo $details['varPropertyName']; ?></div>
        <div style="margin-bottom: 15px; "><a href="<?php echo SITE_PATH.'property/details/'.$details['intGlCode']; ?>" onclick="addViewCnt(<?php echo $details['intGlCode']?>);">All Details</a> | <a href="javascript:void(0)" id="viewCnt" onclick="viewCnt(<?php echo $details['intGlCode']?>);">View Count</a> | <a href="javascript:void(0)" id="viewCnt" onclick="addFavorite(<?php echo $details['intGlCode']?>);">Add as a Favorite</a></div>
        <?php } }else{ ?>
          <div><strong>No record Found.</strong></div>
        <?php } ?>
      </div>

      <?php /* if(count($data) >= DEFAULT_LIMIT){ ?>
      <div id="show_more_main<?php echo count($data); ?>">
          <span id="<?php echo count($data); ?>" class="show_more" title="Load more property">Show more</span>
      </div>
      <?php } */ ?>
      <div class="load-more" data-id="<?php echo count($data); ?>" style="display: none;">
          <img src="<?php echo SITE_PATH."images/ajax-loader.gif";?>"/>
      </div>
      </div>
    </div>
    

    <div class="col-lg-4">
      <div class="row">
        <strong class="property-name">Recently Viewed Property</strong>
        <?php
          if(!empty($recentlyViewedProperty)){
            foreach ($recentlyViewedProperty as $details) { 
            $propertyImages = $this->Module_Model->get_property_images($details['intGlCode'],1);        
          ?>
          <div><img src="<?php echo SITE_PATH.PROPERTY_IMAGES_PATH.$propertyImages['varImageName']; ?>" width="100px" height="100px"></div>
          <div class="property-name"><b>Property Name: </b><?php echo $details['varPropertyName']; ?></div>
          <?php } }else{ ?>
            <div><strong>No recently property available.</strong></div>              
          <?php } ?>
    </div>
    </div>
  </div>
</div>

