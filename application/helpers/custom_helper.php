<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function thumb($fullname, $width, $height,$path) {
    // Path to image thumbnail in your root
    $dir = $path;
    $url = base_url().$path;
    // Get the CodeIgniter super object
    $CI = &get_instance();
    // get src file's extension and file name
    $extension = pathinfo($fullname, PATHINFO_EXTENSION);
    $filename = pathinfo($fullname, PATHINFO_FILENAME);
    $image_org = $dir . $filename . "." . $extension;
    $image_thumb = $dir . $filename . "-" . $height . '_' . $width . "." . $extension;
    $image_returned = $url . $filename . "-" . $height . '_' . $width . "." . $extension;

    if (!file_exists($image_thumb)) {
        // LOAD LIBRARY
        $CI->load->library('image_lib');
        // CONFIGURE IMAGE LIBRARY
        $config['source_image'] = $image_org;
        $config['new_image'] = $image_thumb;
        $config['width'] = $width;
        $config['height'] = $height;
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        $CI->image_lib->clear();
    }
    return $image_returned;
}

/* End of file image_helper.php */
/* Location: ./application/helpers/image_helper.php */
