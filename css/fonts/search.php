<?php

class search extends Front_Controller{
    function __construct() {
            parent::__construct();
            $this->load->model('search_model', 'Module_Model');
            $this->search_listing_tpl = 'search_listing_tpl';
        }
    
	public function index(){
            
            $this->Search_Text = str_replace("'","''",trim($this->input->get_post('SearchText',true)));
            $this->load_data();
            $this->viewData['Search_Text'] = $this->Search_Text;
            $this->viewData['ContentPanel'] = $this->search_listing_tpl;
            $this->load_view();
	}
       
        public function load_data(){
            // echo RECORD_ID;exit;
            //echo $this->record_id;exit;
            
             $Count = $this->Module_Model->Select_Count($this->Search_Text);
             if($Count == 0){
                 $this->Module_Model->InsertintoFailedKeywords($this->Search_Text);
             }
             $this->Module_Model->NumOfRows = $Count;
             $this->Module_Model->initialize_front();
             $this->viewData['Total_Count'] = $Count;
             $Search = $this->Module_Model->Select_Search_Name(RECORD_ID);
             $this->viewData['PageName'] = $Search['varTitle'];
             $this->viewData['PageDesc'] = $this->common_model->CharacterLimiter($Search['txtDescription'],200);
             $this->viewData['Search_Text'] = $this->Search_Text;
             $Banner = $this->Module_Model->Select_Search_Banner(RECORD_ID);
             $this->viewData['Banner'] = $Banner;
             $this->viewData['Result'] = $this->Module_Model->Company_Listing($this->Search_Text);
             $this->viewData['Pagging'] = $this->mylibrary->generatepagingfront($this, 'Top');
             if ($this->input->get_post('ajax', TRUE) == 'Y') {
                echo $this->parser->parse($this->search_listing_tpl, $this->viewData);
                exit;
            }
        }
        
        function UpdateHits(){
            $this->Module_Model->UpdateHits();
        }
        
}

?>
