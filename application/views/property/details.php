<script type="text/javascript" src="<?php echo SITE_PATH."js/jquery.min.js"; ?>"></script>
<script type="text/javascript" src="<?php echo SITE_PATH."js/lightslider.js"; ?>"></script>
<link href="<?php echo SITE_PATH."css/lightslider.css"; ?>" rel="stylesheet" type="text/css"/>
<style>
    	ul{
			list-style: none outside none;
		    padding-left: 0;
            margin: 0;
		}
        .demo .item{
            margin-bottom: 60px;
        }
		.content-slider li{
		    background-color: #ed3020;
		    text-align: center;
		    color: #FFF;
		}
		.content-slider h3 {
		    margin: 0;
		    padding: 70px 0;
		}
		.demo{
			width: 800px;
		}
		.lSGallery{display: none;}

		.col-lg-6.property-data div{padding: 5px;}
    </style>
<script>
 	$(document).ready(function() {
		// $("#content-slider").lightSlider({
  //           loop:true,
  //           keyPress:true
  //       });
        $('#image-gallery').lightSlider({
            gallery:true,
            item:1,
            thumbItem:0,
            slideMargin: 0,
            speed:500,
            auto:true,
            loop:true,
            onSliderLoad: function() {
                $('#image-gallery').removeClass('cS-hidden');
            }  
        });
	});
</script>



<?php 
if(!empty($propertyImageData)){ ?>
<div class="demo">
    <div class="item">            
        <div class="clearfix" style="max-width:474px;">
			<ul id="image-gallery" class="gallery list-unstyled cS-hidden">
			<?php foreach($propertyImageData as $value){ ?>	
		        <li><img src="<?php echo SITE_PATH.PROPERTY_IMAGES_PATH.$value['varImageName']; ?>" /></li>
			<?php } ?>
			</ul>
		</div>
	</div>
</div>
<?php } ?>

<div class="row">
    <div class="col-lg-12">           
        <h2>Property Details:</h2>
     </div>
</div>

<?php 
// print_r($propertyData);
if(!empty($propertyData)){ ?>
	<div class="row">
		<div class="col-lg-6 property-data">
			<div><strong>Property Name: </strong><?php echo $propertyData['varPropertyName']; ?></div>
			<div><strong>Discription: </strong><?php echo $propertyData['varDescription']; ?></div>
			<div><strong>Address: </strong><?php echo $propertyData['varAddress']; ?></div>
			<div><strong>Locality/Area: </strong><?php echo $propertyData['varLocality']; ?></div>
			<div><strong>Carpet Area: </strong><?php echo $propertyData['varCarpetArea']; ?></div>
			<div><strong>Price: </strong><?php echo $propertyData['decPrice']; ?></div>
			<div><strong>Bed Room: </strong><?php echo $propertyData['intBedRoom']; ?></div>
			<div><strong>Bath: </strong><?php echo $propertyData['intBath']; ?></div>
			<div><strong>Viewed Count: </strong><?php echo $propertyData['intViewCount']; ?></div>
			<div><strong>Favorite Status: </strong><?php echo $propertyData['intFavorite'] == 1 ? 'Yes' : 'No'; ?></div>
		</div>
	</div>
<?php } ?>