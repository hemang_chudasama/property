<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends CI_Controller {

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function __construct() {
        //load database in autoload libraries 
        parent::__construct();
        $this->load->database();
        $this->load->library('image_lib');
        $this->load->library('upload');
        $this->load->helper('image_helper');
        // $this->load->helper('custom_helper');
        $this->load->model('PropertyModel', 'Module_Model');
    }

    public function index() {
        $property = new PropertyModel;
        $data['data'] = $property->get_property();
        $data['recentlyViewedProperty'] = $this->Module_Model->recentlyViewedProperty();
        $this->load->view('includes/header');
        $this->load->view('property/list', $data);
        $this->load->view('includes/footer');
    }

    public function create() {
        $this->load->view('includes/header');
        $this->load->view('property/create');
        $this->load->view('includes/footer');
    }

    public function showmore(){
        $id = $this->input->post('id');
        if(!empty($id)){
            $totalRowCount = $this->Module_Model->countData($id);
            $loadMoreData = $this->Module_Model->loadMoreDate($id);
            if($totalRowCount > 0 && !empty($loadMoreData)){
                foreach ($loadMoreData as $key => $value) {
                    $propertyImages=$this->Module_Model->get_property_images($value['intGlCode'],1);
                    $html = '';
                    $html .= "<div><img src='". SITE_PATH.PROPERTY_IMAGES_PATH.$propertyImages['varImageName']."' width='100px' height='100px'></div>";
                    $html .= "<div><b>Property Name: </b>".$value['varPropertyName']."</div>";
                    $html .= "<div><a href='".SITE_PATH.'property/details/'.$value['intGlCode']."'>All Details</a> | <a href='javascript:void(0)' id='viewCnt' onclick='viewCnt('".$value['intGlCode']."')'>View Count</a> | <a href='javascript:void(0)' id='viewCnt' onclick='addFavorite('".$value['intGlCode']."');'>Add as a Favorite</a></div>";
                    // $html .= "<div class='show_more_main' id='show_more_main".count($totalRowCount)."'>";                    
                    echo $html;      
                }                   
            }            
        }     
    }

    public function search(){
        $searchData = $this->Module_Model->getPropertySearch();
        if(!empty($searchData)){
            $html = '';
            $html .= '<div class="col-lg-4 loadMoreData">';
            foreach ($searchData as $key => $value) {
                $propertyImages = $this->Module_Model->get_property_images($value['intGlCode'],1);
                
                $html .= '<div><img src="'.SITE_PATH.PROPERTY_IMAGES_PATH.$propertyImages['varImageName'].'" width="100px" height="100px"></div>';
                $html .= '<div class="property-name"><b>Property Name: </b>'.$value['varPropertyName'].'</div>';

                $html .= '<div style="margin-bottom: 15px; "><a href="'.SITE_PATH."property/details/".$value['intGlCode'].'" onclick="addViewCnt('.$value['intGlCode'].');">All Details</a> | <a href="javascript:void(0)" id="viewCnt" onclick="viewCnt('.$value['intGlCode'].');">View Count</a> | <a href="javascript:void(0)" id="viewCnt" onclick="addFavorite('.$value['intGlCode'].');">Add as a Favorite</a></div>';
            }
            $html .= '</div>';
            
        }else{
            $html = '<div class="col-lg-4"><strong>No record found.<strong></div>';
        }
        echo $html;die;
    }

    public function addViewCnt(){    
        $id = $this->input->post('id');
        $this->Module_Model->addViewCount($id);
    }


    public function addFavorite(){    
        $id = $this->input->post('id');
        echo $this->Module_Model->addFavorite($id);
    }

     public function getViewCnt(){    
        $id = $this->input->post('id');
        echo $this->Module_Model->getViewCount($id);
    }

    public function details($id){
        if(!empty($id)){            
            $property = new PropertyModel;
            $propertyImageData = $property->getAllPropertyImages($id);
            $propertyData = $property->getPropertyById($id);

            $this->viewData['propertyImageData'] = $propertyImageData;
            $this->viewData['propertyData'] = $propertyData;
            $this->load->view('property/details',$this->viewData);
        }else{            
            redirect(base_url('property'));
        }
    }

    /**
     * Store Data from this method.
     *
     * @return Response
     */
    public function store() {
        $property = new PropertyModel;
        $property->insertPropertyData();
        redirect(base_url('property'));
    }

}
